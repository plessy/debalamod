# debalamod

Deb à la mod – Provide installed Debian packages as environment modules.

## Purpose

I wrote this tool to provide [Debian Med packages](https://blends.debian.org/med/tasks/bio)
as [Lmod environment modules](https://lmod.readthedocs.io) on the [high-performance computing
cluster](https://groups.oist.jp/scs/deigo) at my workplace, [OIST](https://www.oist.jp/).
I hope it can be useful to others.

## The workflow in brief:

 - Create a singularity image containing the packages of interest and the
   `debalamod` script.
 - Copy the image on your HPC cluster.
 - Run `debalamod` from the image to install the module configuration files and
   shortcut scripts that it produces.
 - Load the modules you like to enjoy Debian-pacakged software on your HPC
   cluster.

## Hello example

```
singularity build --fakeroot debalamod.sif debalamod.docker.def
```

Then copy on your HPC cluster and run something like:

```
# ml singularity # if needed on your cluster
for PKG in $(./debalamod.sif cat /debalamod_pkg_list)
do
  ./debalamod.sif /debalamod $PKG
done
```

Load and run the `hello` module:

```
ml use modulefiles
ml hello
hello
```

## If you do not have Lmod installed on your cluster

The image also provides the `ml` and `module` commands (at the moment for
_bash_ shells).  You can export them with:

```
./debalamod.sif /debalamod init_lmod
. lmod/init_lmod
```
